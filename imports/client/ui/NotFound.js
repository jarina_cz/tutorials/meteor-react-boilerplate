import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const NotFound = () => {
  return (
    <div className="boxed-view">
      <div className="boxed-view__box">
        <h1>Page Not Found</h1>
        <p>Hmmm, we're unable to find that page.</p>
        <Link to="/" className="button button--link">HEAD HOME</Link>
      </div>
    </div>
  );
};

NotFound.propTypes = {};
NotFound.defaultProps = {};

export default NotFound;
