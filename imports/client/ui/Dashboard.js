import React from 'react';
import PropTypes from 'prop-types';

import PrivateHeader from './PrivateHeader';

const Link = () => {
  return (
    <div>
      <PrivateHeader title="Dashboard"/>
      <div className="page-content">
        Dashboard page content.
      </div>
    </div>
  );
};

Link.propTypes = {};
Link.defaultProps = {};

export default Link;